/**
 *      File:           mandelbrot.c
 *      Date:           28/01/2011 & 02/04/2012  
 *                          ■ Base version of file containing a server 
 *                            that serves a BMP. 
 *                      19/04/2016
 *                          ■ Edited version that allows a tile of the
 *                            mandelbrot set to be viewed.
 *      Author:         Gautham Ontri [gauthamontri] (z5117283)
 *                      Pang Je Ho [pangjeho] (z5117924)
 *                          ■ Combined effort for the creation of the 
 *                            mandelbrot set viewer
 *                      Tim Lambert & Richard Buckland
 *                          ■ Provided the base version of the bmpServer
 *                      Jumail Mudekkat (OpenLearning)
 *                          ■ Providing fixes to simpleServer to enable
 *                            multiplatform compilation.
 *                          
 *      Description:    
 *      
 *          A program which, when compiled and ran will create a web
 *          server that serves either a single tile of the mandelbrot
 *          set or an entire viewer.
 */


#define PLATFORM_WINDOWS  1
#define PLATFORM_MAC      2
#define PLATFORM_UNIX     3

#if defined(_WIN32 ) 
    #define PLATFORM PLATFORM_WINDOWS
 
#elif defined(__APPLE__ ) 
    #define PLATFORM PLATFORM_MAC
 
#else
    #define PLATFORM PLATFORM_UNIX

#endif

////////////////////////////
//    Header Libraries    //
////////////////////////////
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <math.h>
#include "mandelbrot.h" // include both of the .h files
#include "pixelColor.h" // make sure they're spelled right!

#if PLATFORM == PLATFORM_WINDOWS

    #include <winsock2.h>

#elif PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX

    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <fcntl.h>

#endif


////////////////////////////
//   Function Protoypes   //
////////////////////////////
int waitForConnection( int serverSocket );
int makeServerSocket( int portno );
void zoomRegion( double x, double y, int levels );
void createHeader( int socket );

void serveBitmap( int socket, double x, double y, int z );
void serveViewer( int socket );

////////////////////////////
//  Constants or Macros   //
////////////////////////////
#define SIMPLE_SERVER_VERSION       1.1
#define REQUEST_BUFFER_SIZE         1000
#define DEFAULT_PORT                8779         
//The port for connection and URL
#define NUMBER_OF_PAGES_TO_SERVE    11111
//After serving this many pages the server will halt

#define MAGICHEADER                 0x4d42
#define BYTES_PER_PIXEL             3
#define OFFSET                      54
#define NO_COMPRESSION              0
#define IMAGESIZE                   512
#define DIB_HEADER_SIZE             40
#define PIX_PER_METRE               2835
#define NUMBER_PLANES               1
#define NUM_COLORS                  0

#define SIZE                        512
#define MAX_STEPS                   256

typedef unsigned char oneByte;
typedef unsigned short twoBytes;
typedef unsigned int fourBytes;

int main( int argc, char *argv[] )  {
      
    printf( "Mandelbrot Set Viewer Server [%f], started.\n", 
            SIMPLE_SERVER_VERSION ); 
 
    printf( "It may be accessed at: http://localhost:%d/\n", 
             DEFAULT_PORT );

    int serverSocket = makeServerSocket( DEFAULT_PORT ); 

    char request[ REQUEST_BUFFER_SIZE ];

    int numberServed = 0;
    while( numberServed < NUMBER_OF_PAGES_TO_SERVE )  {
      
        printf( "%d out of %d pages served.\n", numberServed, 
                 NUMBER_OF_PAGES_TO_SERVE );

        int connectionSocket = waitForConnection( serverSocket );
        // wait for a request to be sent from a web browser, open a new
        // connection for this conversation

        // read the first line of the request sent by the browser  
        int bytesRead;
        bytesRead = recv( connectionSocket, request,( sizeof request ) -1,
                          0 );

        assert( bytesRead >= 0 ); 
        // were we able to read any data from the connection?
            
        // print entire request to the console 
        printf( "HTTP Request received.\n %s\n", request );

        //send the browser a simple html page using http

        double x = 0;
        double y = 0;
        int z = 0;
        int reqInputs = 0;

        reqInputs = sscanf( request, "GET /tile_x%lf_y%lf_z%d.bmp", 
                            &x, &y, &z );

        if( reqInputs == 3 ) {
            serveBitmap( connectionSocket, x, y, z );
            printf( "X: %f, Y: %f, Zoom: %d", x, y, z );
        } else {
            serveViewer( connectionSocket );
        }

        // close the connection after sending the page- keep aust beautiful
        close( connectionSocket );
      
        numberServed++;
    } 

   
   // close the server connection after we are done- keep aust beautiful
    printf( "The server is shutting down.\n" );
    #if PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
        close( serverSocket );
    #elif PLATFORM == PLATFORM_WINDOWS
        closesocket( serverSocket  );
        WSACleanup( );
    #endif
    return EXIT_SUCCESS; 
}

/**
 * @brief      { This function calculates the amount of steps needed from
 *               a given coordinate to escape the circle of radius 2. }
 *
 * @param[in]  x     { X Coordinate }
 * @param[in]  y     { Y Coordinate }
 *
 * @return     { (int) The amount of steps required to escape, with a
 *               maximum of 256 steps being calculated. }
 */
int escapeSteps( double x, double y  )  {
    //This function was adapted from the pseudocode from:
    //https://en.wikipedia.org/wiki/Mandelbrot_set
    
    int steps = 0;

    double Re = 0.0;
    double Im = 0.0;

    while( Re*Re + Im*Im <= 4 && steps < MAX_STEPS  )  {
        double nextRe =(  Re * Re  )  - Im * Im + x;
        double nextIm = 2 * Re * Im + y;

        Re = nextRe;
        Im = nextIm;

        steps++;
    }
    return steps;
}

void serveViewer( int socket ) { 
    char* msg; 
    msg =
        "HTTP/1.0 200 Found\n"
        "Content-Type: text/html\n"
        "\n";
    write( socket, msg, strlen( msg ) );

    msg =
    "<!DOCTYPE html>\n"
    "<script src=\"http://almondbread.cse.unsw.edu.au/tiles.js\"></script>"
    "\n";      
    write( socket, msg, strlen( msg ) );
}

/**
 * @brief      { This function will serve a 512*512 tile (bitmap) of the 
 *               mandelbrot set. }
 *
 * @param[in]  socket  { The socket number required for 'write' }
 * @param[in]  x       { X Coordinate }
 * @param[in]  y       { Y Coordinate }
 * @param[in]  z       { Zoom Level }
 */
void serveBitmap( int socket, double x, double y, int z ) {
    char *msg;

    msg =   "HTTP/1.0 200 OK\r\n"
            "Content-Type: image/bmp\r\n"
            "\r\n";

    write( socket, msg, strlen( msg ) );
    
    createHeader( socket );
    //Write response headers then bitmap headers.
    double scaleInc = pow( 2, -z );
    double initialX = x - (0.5*SIZE*scaleInc);
    double initialY = y - (0.5*SIZE*scaleInc);

    double finalX = x + (0.5*SIZE*scaleInc);
    double finalY = y + (0.5*SIZE*scaleInc);

    double countX = initialX;
    double countY = initialY;

    printf( "iX: %f, iY: %f, fX: %f, fY: %f\n", initialX, initialY, finalX, finalY);

    int escSteps = 0;
    unsigned char Colour[ 3 ] = { 0 ,0, 0 };
    while( countY < finalY ) {
        while( countX < finalX ) {
            escSteps = escapeSteps( countX, countY );
            Colour[ 0 ] = stepsToBlue( escSteps );//B
            Colour[ 1 ] = stepsToGreen( escSteps );//G
            Colour[ 2 ] = stepsToRed( escSteps );//R
            write( socket, Colour, sizeof Colour);
            countX += scaleInc;
        }
        countX = initialX;
        countY += scaleInc; 
    }
    /*for( countY = initialY; countY < finalY; countY += scaleInc ) {
        for( countX = initialX; countX < finalX; countX += scaleInc ) {
            escSteps = escapeSteps( countX, countY );
            memcpy( Colour, returnColour( escSteps ), 3 );
            write( socket, Colour, sizeof Colour);
        }
    }*/
}

/**
 * @brief      { This function will append a bitmap-header. }
 *
 * @param[in]  socket  { The socket number required for 'write' }
 */
void createHeader( int socket ) {

    twoBytes magicNumber = MAGICHEADER;
    write( socket, &magicNumber, sizeof magicNumber );

    fourBytes fileSize = OFFSET +( IMAGESIZE * IMAGESIZE * 
                         BYTES_PER_PIXEL );
    write( socket, &fileSize, sizeof fileSize );

    fourBytes reserved = 0;
    write( socket, &reserved, sizeof reserved );

    fourBytes offset = OFFSET;
    write( socket, &offset, sizeof offset );

    fourBytes dibHeaderSize = DIB_HEADER_SIZE;
    write( socket, &dibHeaderSize, sizeof dibHeaderSize );

    fourBytes width = IMAGESIZE;
    write( socket, &width, sizeof width );

    fourBytes height = IMAGESIZE;
    write( socket, &height, sizeof height );

    twoBytes planes = NUMBER_PLANES;
    write( socket, &planes, sizeof planes );

    twoBytes bitsPerPixel = BYTES_PER_PIXEL * 8;
    write( socket, &bitsPerPixel, sizeof bitsPerPixel );

    fourBytes compression = NO_COMPRESSION;
    write( socket, &compression, sizeof compression );

    fourBytes imageSize =( IMAGESIZE * IMAGESIZE * BYTES_PER_PIXEL );
    write( socket, &imageSize, sizeof imageSize );

    fourBytes hResolution = PIX_PER_METRE;
    write( socket, &hResolution, sizeof hResolution );

    fourBytes vResolution = PIX_PER_METRE;
    write( socket, &vResolution, sizeof vResolution );

    fourBytes numColors = NUM_COLORS;
    write( socket, &numColors, sizeof numColors );

    fourBytes importantColors = NUM_COLORS;
    write( socket, &importantColors, sizeof importantColors );
}



/**
 * @brief      { This function will start the server listening on the 
 *               specified port number }
 *
 * @param[in]  portNumber  { The port number to start }
 *
 * @return     { The serverSocket number required for multiple network 
 *               functions }
 */
int makeServerSocket( int portNumber )  {
   
    #if PLATFORM == PLATFORM_WINDOWS
        WSADATA WsaData;
        assert( ! ( WSAStartup( MAKEWORD( 2,2 ) , &WsaData ) ) );
    #endif
 
    // create socket
    int serverSocket = socket( AF_INET, SOCK_STREAM, 0 );
    assert( serverSocket >= 0 );
    // check there was no error in opening the socket
 
    // bind the socket to the listening port ( 7191 in this case ) 
    struct sockaddr_in serverAddress;
    serverAddress.sin_family      = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port        = htons( portNumber );
 
    // tell the server to restart immediately after a previous shutdown
    // even if it looks like the socket is still in use
    // otherwise we might have to wait a little while before rerunning the
    // server once it has stopped
    
    #if PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
        const int optionValue   = 1;
    #elif PLATFORM == PLATFORM_WINDOWS
        const char optionValue  = 1;
    #endif
    
    setsockopt( serverSocket, SOL_SOCKET, SO_REUSEADDR, &optionValue, sizeof( int )  );
 
    int bindSuccess = bind( serverSocket,( struct sockaddr* ) &serverAddress, sizeof( serverAddress )  );
 
    assert( bindSuccess >= 0 );
    // if this assert fails wait a short while to let the operating
    // system clear the port before trying again
 
    return serverSocket;
}

/**
 * @brief      { This function will wait for a browser to request a 
 *               connection then return an appropriate socket. }
 *
 * @param[in]  serverSocket  { The socket number required for network 
 *                             functions }
 *
 * @return     { The socket on which the conversation will take place }
 */
int waitForConnection( int serverSocket )  {
 
    // listen for a connection
    const int serverMaxBacklog = 10;
    listen( serverSocket, serverMaxBacklog );
    
    #if PLATFORM == PLATFORM_WINDOWS
        typedef int socklen_t;
    #endif
 
    // accept the connection
    struct sockaddr_in clientAddress;
    socklen_t clientLen = sizeof( clientAddress );
    int connectionSocket = accept( serverSocket,( struct sockaddr* ) &clientAddress, &clientLen );
    assert( connectionSocket >= 0 );
    // check for connection error
 
    return connectionSocket;
}
