/**
 *      File:           pixelColor.c
 *      Date:           28/01/2011 & 02/04/2012  
 *                          ■ Base version of file containing a server 
 *                            that serves a BMP. 
 *                      19/04/2016
 *                          ■ Edited version that allows a tile of the
 *                            mandelbrot set to be viewed.
 *      Author:         Gautham Ontri [gauthamontri] (z117283)
 *                      Pang Je Ho [pangjeho] (z5117924)
 *                          ■ Combined effort for the creation of the 
 *                            mandelbrot set viewer
 *                      Tim Lambert & Richard Buckland
 *                          ■ Provided the base version of the bmpServer
 *                      Jumail Mudekkat (OpenLearning)
 *                          ■ Providing fixes to simpleServer to enable
 *                            multiplatform compilation.
 *                          
 */


#include <stdio.h>
#include <stdlib.h>

#include "pixelColor.h"


#define MAX_STEPS 256

 /**
 * @brief      { This function will return a colour based on the steps
 *               taken for an input to escape the mandelbrot set. }
 *
 * @param[in]  steps  { The steps taken to escape the mandelbrot set. }
 *
 * @return     { (char) Each character contains the values for RGB }
 */

unsigned char stepsToRed(int steps) {
	int redColor = steps; 
    /*if( steps == MAX_STEPS ) { 
		redColor = 0;
    } else if( steps > MAX_STEPS*0.5 && steps < MAX_STEPS*0.9 ) {
		redColor = steps;
    } else if( steps == 0 ) {
		redColor = 255;
    }*/

	return redColor;
}


unsigned char stepsToGreen(int steps) {
	int greenColor = 0;
    /*if( steps == MAX_STEPS ) { 
		greenColor = 0;
    } else if( steps > MAX_STEPS*0.5 && steps < MAX_STEPS*0.9 ) {
		greenColor = steps;
    } else if( steps == 0 ) {
		greenColor = 255;
    }*/

	return greenColor;
}


unsigned char stepsToBlue(int steps) {
    int blueColor = steps;
    if( steps == MAX_STEPS ) {
    	blueColor = 0;
    } else if( steps > MAX_STEPS*0.7 ) {
    	blueColor = rand() % 50;
    }/*if( steps == MAX_STEPS ) { 
		blueColor = 0;
    } else if( steps > MAX_STEPS*0.5 && steps < MAX_STEPS*0.9 ) {
		blueColor = steps;
    } else if( steps == 0 ) {
		blueColor = 255;
    }*/

	return blueColor;
}
