/**
 *      File:           xanadu.h
 *      Date:           28/01/2011 & 02/04/2012  
 *                          ■ Base version of file containing a server 
 *                            that serves a BMP. 
 *                      19/04/2016
 *                          ■ Edited version that allows a tile of the
 *                            mandelbrot set to be viewed.
 *      Author:         Gautham Ontri [gauthamontri] (z5117283)
 *                      Pang Je Ho [pangjeho] (z5117924)
 *                          ■ Combined effort for the creation of the 
 *                            mandelbrot set viewer
 *                      Tim Lambert & Richard Buckland
 *                          ■ Provided the base version of the bmpServer
 *                      Jumail Mudekkat (OpenLearning)
 *                          ■ Providing fixes to simpleServer to enable
 *                            multiplatform compilation.
 *                          
 */

#define CENTER_X	-0.48316045
#define CENTER_Y	0.625536794
#define	ZOOM		15